use crate::get_random_suffix;
use crate::ICON_BASE_URL;
use crate::NAMESPACE;
use crate::QUIET;
use crate::RENDER_WITH_ICONS;
use crate::Resource;


pub fn dispatch_output(resource: &Resource) {
    match String::from(&resource.kind).trim() {
        "ConfigMap" => show_configmap(&resource),
        "Deployment" => show_deployment(&resource),
        // "Ingress" => show_ingress(&resource),
        "HorizontalPodAutoscaler" => show_horizontalpodautoscaler(&resource),
        "PersistentVolumeClaim" => show_persistentvolumeclaim(&resource),
        "Secret" => show_secret(&resource),
        "Service" => show_service(&resource),
        "ServiceAccount" => show_serviceaccount(&resource),
        _ => show_other(&resource),
    };
}

pub fn show_header() {
    if !*QUIET {
        println!("");
        println!("────────────────────────────────────────────────────────────────────────────────");
        println!("To generate your diagram please copy/paste the following code");
        println!("in the D2 Playground Editor at https://play.d2lang.com");
        println!("────────────────────────────────────────────────────────────────────────────────");
    }
    println!("direction: right");
    println!("namespace: {{");
}

pub fn show_footer() {
    println!("}}");
    if !*QUIET {
        println!("────────────────────────────────────────────────────────────────────────────────");
    }
}

// Return svg icon of the resource
fn get_resource_icon(resource_short_name: &str) -> String {
    format!(
        "{{shape: image; icon: {}/{}.svg}}",
        // "{}/{}.svg",
        ICON_BASE_URL, resource_short_name
    )
}

fn show_configmap(resource: &Resource) {
    println!(
        "{}: |md\n{}\n({})\n|",
        &resource.key,
        &resource.name,
        &resource.kind
    );
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}: {}",
            &resource.key,
            get_resource_icon("cm"),
        );
    }
}

fn show_deployment(resource: &Resource) {
    println!(
        "{}: |md\n{}\n({})\n|",
        &resource.key,
        &resource.name,
        &resource.kind
    );
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}: {}",
            &resource.key,
            get_resource_icon("deploy"),
        );
    }

    // Get "spec.replicas" property
    let value_spec_replicas = serde_yaml::to_string(&resource.yaml["spec"]["replicas"]);
    let spec_replicas = match value_spec_replicas {
        Ok(spec_replicas) => {
            if spec_replicas.trim() == "null" {
                String::from("0")
            } else {
                String::from(spec_replicas.trim())
            }
        }
        Err(e) => {
            eprintln!("Error: {}", e);
            std::process::exit(exitcode::DATAERR);
        }
    };

    // Create ReplicaSet
    let rs_suffix = get_random_suffix(8);
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}_rs: {}{} ({})",
            &resource.key,
            &resource.name,
            rs_suffix,
            "ReplicaSet"
        );
        println!(
            "{}_rs: {}",
            &resource.key,
            get_resource_icon("rs"),
        );
    } else {
        println!(
            "{}_rs: {}{} ({})",
            &resource.key,
            &resource.name,
            rs_suffix,
            "ReplicaSet"
        );
    }
    println!(
        "{} -> {}_rs",
        &resource.key, &resource.key
    );

    // Concatenate all pods in one
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}_pod: {}{} ({})",
            &resource.key,
            &resource.name,
            rs_suffix,
            "Pod"
        );
        println!(
            "{}_pod: {}",
            &resource.key,
            get_resource_icon("pod"),
        );
    } else {
        println!(
            "{}_pod: {}{} ({})",
            &resource.key,
            &resource.name,
            rs_suffix,
            "Pod"
        );
    }
    println!("{}_rs -> {}_pod: supervise", &resource.key, &resource.key);

    // Create link Pod ==> ServiceAccount
    let value_spec_serviceaccountname =
        serde_yaml::to_string(&resource.yaml["spec"]["template"]["spec"]["serviceAccountName"]);
    let spec_serviceaccountname = match value_spec_serviceaccountname {
        Ok(spec_serviceaccountname) => String::from(spec_serviceaccountname.trim()),
        Err(e) => {
            eprintln!("Error: {}", e);
            std::process::exit(exitcode::DATAERR);
        }
    };
    if !spec_serviceaccountname.is_empty() {
        println!(
            "{}_pod -> {}_ServiceAccount_{}: executed as {{
                style.stroke-dash: 3
              }}",
            &resource.key, &resource.namespace, spec_serviceaccountname
        )
    }

    // Create link Deployment ==> EnvFrom
    let containers_sequence = &resource.yaml["spec"]["template"]["spec"]["containers"]
        .as_sequence()
        .unwrap();
    for (_i, container) in containers_sequence.iter().enumerate() {
        let envfroms_sequence = match container["envFrom"].as_sequence() {
            Some(envfroms_sequence) => envfroms_sequence,
            None => continue,
        };

        for (_i, envfrom) in envfroms_sequence.iter().enumerate() {
            // Create link Deployment ==> EnvFrom ConfigMap
            let yaml_as_str = serde_yaml::to_string(&envfrom["configMapRef"]["name"]).unwrap();
            if !(yaml_as_str.trim().is_empty() || yaml_as_str.trim() == "null") {
                println!(
                    "{}_pod -> {}_ConfigMap_{}: inject as environment variables",
                    &resource.key,
                    &resource.namespace,
                    yaml_as_str.trim()
                );
            }

            // Create link Deployment ==> EnvFrom Secret
            let yaml_as_str = serde_yaml::to_string(&envfrom["secretRef"]["name"]).unwrap();
            if !(yaml_as_str.trim().is_empty() || yaml_as_str.trim() == "null") {
                println!(
                    "{}_pod -> {}_Secret_{}: inject as environment variables",
                    &resource.key,
                    &resource.namespace,
                    yaml_as_str.trim()
                );
            }
        }
    }

    // Create link Deployment ==> Volumes
    match &resource.yaml["spec"]["template"]["spec"]["volumes"].as_sequence() {
        Some(volumes_sequence) => {
            for (_i, volume) in volumes_sequence.iter().enumerate() {
                // Create link Deployment ==> ConfigMap
                let yaml_as_str = serde_yaml::to_string(&volume["configMap"]["name"]).unwrap();
                if !(yaml_as_str.trim().is_empty() || yaml_as_str.trim() == "null") {
                    println!(
                        "{}_pod -> {}_ConfigMap_{}: mount as volume",
                        &resource.key,
                        &resource.namespace,
                        yaml_as_str.trim()
                    );
                }

                // Create link Deployment ==> Secret
                let yaml_as_str = serde_yaml::to_string(&volume["secret"]["secretName"]).unwrap();
                if !(yaml_as_str.trim().is_empty() || yaml_as_str.trim() == "null") {
                    println!(
                        "{}_pod -> {}_Secret_{}: mount as volume",
                        &resource.key,
                        &resource.namespace,
                        yaml_as_str.trim()
                    );
                }

                // Create link Deployment ==> PersistentVolumeClaim
                let yaml_as_str =
                    serde_yaml::to_string(&volume["persistentVolumeClaim"]["claimName"]).unwrap();
                if !(yaml_as_str.trim().is_empty() || yaml_as_str.trim() == "null") {
                    println!(
                        "{}_pod -> {}_PersistentVolumeClaim_{}: mount as volume",
                        &resource.key,
                        &resource.namespace,
                        yaml_as_str.trim()
                    );
                }
            }
        }
        None => {}
    };

}

fn show_horizontalpodautoscaler(resource: &Resource) {
    println!(
        "{}: |md\n{}\n({})\n|",
        &resource.key,
        &resource.name,
        &resource.kind
    );
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}: {}",
            &resource.key,
            get_resource_icon("hpa"),
        );
    }
}

fn show_persistentvolumeclaim(resource: &Resource) {
    println!(
        "{}: |md\n{}\n({})\n|",
        &resource.key,
        &resource.name,
        &resource.kind
    );
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}: {}",
            &resource.key,
            get_resource_icon("pvc"),
        );
    }
}

fn show_secret(resource: &Resource) {
    println!(
        "{}: |md\n{}\n({})\n|",
        &resource.key,
        &resource.name,
        &resource.kind
    );
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}: {}",
            &resource.key,
            get_resource_icon("secret"),
        );
    }
}

fn show_service(resource: &Resource) {
    println!(
        "{}: |md\n{}\n({})\n|",
        &resource.key,
        &resource.name,
        &resource.kind
    );
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}: {}",
            &resource.key,
            get_resource_icon("svc"),
        );
    }
}

fn show_serviceaccount(resource: &Resource) {
    println!(
        "{}: |md\n{}\n({})\n|",
        &resource.key,
        &resource.name,
        &resource.kind
    );
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}: {}",
            &resource.key,
            get_resource_icon("sa"),
        );
    }
}

fn show_other(resource: &Resource) {
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}: {} ({})",
            &resource.key,
            &resource.name,
            &resource.kind
        );
    } else {
        println!(
            "{}: {} ({})",
            &resource.key,
            &resource.name,
            &resource.kind
        );
    }
}

// Resolve links beetween Services and Pods from a Deployment
pub fn resolve_label_selectors(resource_vec: &Vec<Resource>) {
    let mut services_vec: Vec<&Resource> = Vec::new();
    let mut deployments_vec: Vec<&Resource> = Vec::new();

    for resource in resource_vec {
        if resource.namespace == *NAMESPACE {
            if resource.kind == "Service" {
                services_vec.push(resource);
            }
            if resource.kind == "Deployment" {
                deployments_vec.push(resource);
            }
        }
    }

    for service in services_vec {
        // Get "spec.selector" property
        let value_spec_selector = serde_yaml::to_value(&service.yaml["spec"]["selector"]);
        let spec_selector = match value_spec_selector {
            Ok(spec_selector) => spec_selector,
            Err(e) => {
                eprintln!("Error: {}", e);
                std::process::exit(exitcode::DATAERR);
            }
        };

        for deployment in &deployments_vec {
            // Get "spec.selector.matchLabels" property
            let value_d_spec_selector =
                serde_yaml::to_value(&deployment.yaml["spec"]["selector"]["matchLabels"]);
            let spec_d_selector = match value_d_spec_selector {
                Ok(spec_d_selector) => spec_d_selector,
                Err(e) => {
                    eprintln!("Error: {}", e);
                    std::process::exit(exitcode::DATAERR);
                }
            };

            // Create link Service ==> Pod
            if spec_selector == spec_d_selector {
                println!("{} -> {}_pod: target", service.key, deployment.key);
            }
        }
    }
}
