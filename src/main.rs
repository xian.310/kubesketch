use clap::Parser;
use log::info;
use rand::distributions::Alphanumeric;
use rand::Rng;
use serde_yaml::Value;
use std::env;
use std::fs::File;
use std::io;
use std::io::{stdin, BufRead, BufReader};
mod d2;
mod mermaid;

// icons source: https://github.com/kubernetes/community/tree/master/icons/svg/resources/labeled
static ICON_BASE_URL: &str =
    "https://raw.githubusercontent.com/kubernetes/community/master/icons/svg/resources/labeled";

// https://www.sitepoint.com/rust-global-variables/
#[macro_use]
extern crate lazy_static;

lazy_static! {
    static ref NAMESPACE: String = set_conf_namespace();
    static ref RENDER_FORMAT: OutputFormat = set_conf_render_format();
    static ref RENDER_WITH_ICONS: bool = set_conf_render_with_icons();
    static ref QUIET: bool = set_conf_quiet();
    static ref VERBOSE: bool = set_conf_verbose();
}

/// Specify command line arguments
#[derive(Parser, Debug)]
#[command(version = "0.2.0")]
struct Args {
    /// The path to the file to read, use - to read from stdin (must not be a tty)
    path: std::path::PathBuf,

    /// Kubernetes namespace
    #[arg(short, long, default_value_t = String::from("default"))]
    namespace: String,

    /// Output format
    #[arg(short, long, default_value_t = String::from("mermaid"))]
    format: String,

    /// Use official kubernetes resources icons
    #[arg(short, long, default_value_t = false)]
    icons: bool,

    /// Show only diagram
    #[arg(short, long, default_value_t = false)]
    quiet: bool,

    /// Verbose mode
    #[arg(short, long, default_value_t = false)]
    verbose: bool,
}

fn set_conf_render_with_icons() -> bool {
    let args = Args::parse();
    args.icons
}

fn set_conf_render_format() -> OutputFormat {
    let args = Args::parse();
    match args.format.as_str() {
        "mermaid" => OutputFormat::Mermaid,
        "d2" => OutputFormat::D2,
        _ => OutputFormat::Mermaid        
    }

}

fn set_conf_quiet() -> bool {
    let args = Args::parse();
    args.quiet
}

fn set_conf_verbose() -> bool {
    let args = Args::parse();
    args.verbose
}

fn set_conf_namespace() -> String {
    let args = Args::parse();
    args.namespace
}

#[derive(Debug)]
enum OutputFormat {
    Mermaid,
    D2,
}

#[derive(Debug)]
pub struct Resource {
    key: String,
    kind: String,
    name: String,
    namespace: String,
    yaml: serde_yaml::Value,
}

impl Resource {
    // Construct struct type Resource
    fn build_resource(yaml: serde_yaml::Value) -> Resource {
        // Récupère la propriété "kind"
        let value_kind = serde_yaml::to_string(&yaml["kind"]);
        let kind = match value_kind {
            Ok(kind) => String::from(kind.trim()),
            Err(e) => {
                eprintln!("Error: {}", e);
                std::process::exit(exitcode::DATAERR);
            }
        };

        // Get "metadata.name" property
        let value_name = serde_yaml::to_string(&yaml["metadata"]["name"]);
        let name = match value_name {
            Ok(name) => String::from(name.trim()),
            Err(e) => {
                eprintln!("Error: {}", e);
                std::process::exit(exitcode::DATAERR);
            }
        };

        // Get "metadata.namespace" property
        let value_namespace = serde_yaml::to_string(&yaml["metadata"]["namespace"]);
        let namespace = match value_namespace {
            Ok(namespace) => {
                if namespace.trim() == "null" {
                    String::from("default")
                } else {
                    String::from(namespace.trim())
                }
            }
            Err(e) => {
                eprintln!("Error: {}", e);
                std::process::exit(exitcode::DATAERR);
            }
        };

        Resource {
            key: String::from(namespace.to_owned() + "_" + &kind + "_" + &name),
            kind: kind,
            name: name,
            namespace: namespace,
            yaml: yaml,
        }
    }
}

fn dispatch(resource: &Resource) -> bool {

    if String::from(&resource.namespace).trim() != *NAMESPACE {
        return false;
    }

    match *RENDER_FORMAT {
        OutputFormat::Mermaid => mermaid::dispatch_output(&resource),
        OutputFormat::D2 => d2::dispatch_output(&resource),
    }

    true
}

// Return a random suffix (for dynamic rs or pod suffixes)
fn get_random_suffix(length: usize) -> String {
    let s: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(length)
        .map(char::from)
        .collect();

    String::from(s.to_lowercase())
}

fn main() {
    let args = Args::parse();

    // if Verbose flag is set
    // set the RUST_LOG env var
    if *VERBOSE {
        env::set_var("RUST_LOG", "info");
    }
    env_logger::init();

    info!("Set verbose mode");
    info!("Set namespace: {}", *NAMESPACE);
    info!("Set rendering with icons: {}", *RENDER_WITH_ICONS);
    info!("Set output format: {:?}", *RENDER_FORMAT);

    // Get data as File or stdin
    // Why we use Box: https://phrohdoh.com/blog/read-from-file-or-stdin-rust/
    let path = args.path;
    let reader: Box<dyn io::BufRead> = match path.to_str().unwrap() {
        "-" => {
            info!("Reading from stdin...");
            Box::new(BufReader::new(stdin().lock()))
        }
        _ => {
            info!("Reading from file {:?} ...", path);
            Box::new(BufReader::new(File::open(path).unwrap()))
        }
    };

    // Parse YAML documents
    let mut resources_vec: Vec<Resource> = Vec::new();
    let mut other_namespaces_vec: Vec<String> = Vec::new();
    let mut yaml_str = String::new();
    let mut line_num: i32 = 0;
    for line in reader.lines() {
        let line = line.unwrap();
        line_num = line_num + 1;

        if line.trim() == "---" && !yaml_str.is_empty() {
            // End of a YAML document, we parse it and store it into the vector
            let data: Value = serde_yaml::from_str(&yaml_str).unwrap();
            let resource: Resource = Resource::build_resource(data);
            if resource.namespace == *NAMESPACE {
                resources_vec.push(resource);
            }
            else {
                other_namespaces_vec.push(resource.namespace);
            }
            yaml_str.clear();
        } else {
            // Add the YAML line in the current document
            yaml_str.push_str(&line);
            yaml_str.push('\n');
        }
    }

    // Parse and store the last YAML document
    let data: Value = serde_yaml::from_str(&yaml_str).unwrap();
    let resource: Resource = Resource::build_resource(data);
    if resource.namespace == *NAMESPACE {
        resources_vec.push(resource);
    }
    else {
        other_namespaces_vec.push(resource.namespace);
    }

    other_namespaces_vec.sort();
    other_namespaces_vec.dedup();
    if resources_vec.iter().count() == 0 {
        println!("No resources found in {} namespace.", *NAMESPACE);
        if other_namespaces_vec.iter().count() > 0 {
            println!("Other namespaces found in input: {}.", other_namespaces_vec.join(", "));
        }
        std::process::exit(exitcode::DATAERR);
    }
    else {
        if other_namespaces_vec.iter().count() > 0 {
            if !*QUIET {
                println!("Other namespaces found in input: {}.", other_namespaces_vec.join(", "));
            }
        }
    }

    // Display headers
    match *RENDER_FORMAT {
        OutputFormat::Mermaid => mermaid::show_header(),
        OutputFormat::D2 => d2::show_header(),
    }

    for resource in &resources_vec {
        dispatch(&resource);
    }

    // Display links between selectors
    match *RENDER_FORMAT {
        OutputFormat::Mermaid => mermaid::resolve_label_selectors(&resources_vec),
        OutputFormat::D2 => d2::resolve_label_selectors(&resources_vec),
    }

    // Display footers
    match *RENDER_FORMAT {
        OutputFormat::Mermaid => mermaid::show_footer(),
        OutputFormat::D2 => d2::show_footer(),
    }
}
