use crate::get_random_suffix;
use crate::ICON_BASE_URL;
use crate::NAMESPACE;
use crate::QUIET;
use crate::RENDER_WITH_ICONS;
use crate::Resource;

static ICON_CSS: &str =
    "style='display: block; min-width: 100px; min-height: 100px; margin: auto;'";

pub fn show_header() {
    if !*QUIET {
        println!("");
        println!("────────────────────────────────────────────────────────────────────────────────");
        println!("To generate your diagram please copy/paste the following code");
        println!("in the Mermaid Live Editor at https://mermaid-js.github.io/mermaid-live-editor");
        println!("────────────────────────────────────────────────────────────────────────────────");
    }
    println!("---");
    println!("title: \"namespace: {}\"", *NAMESPACE);
    println!("---");
    println!("graph LR");
}

pub fn show_footer() {
    println!("classDef none            fill:none,stroke:none");
    println!("classDef blue            fill:LightSkyBlue,stroke:SteelBlue");
    println!("classDef green           fill:LightGreen,stroke:SeaGreen");
    println!("classDef red             fill:#f29f9f,stroke:FireBrick");
    println!("classDef grey            fill:#eaeded,stroke:grey");
    println!("classDef subgraph_no_bg  fill:none,stroke:grey");
    println!("class ns, subgraph_no_bg");
    if !*QUIET {
        println!("────────────────────────────────────────────────────────────────────────────────");
    }
}

pub fn dispatch_output(resource: &Resource) {
    match String::from(&resource.kind).trim() {
        "ConfigMap" => show_configmap(&resource),
        "Deployment" => show_deployment(&resource),
        "Ingress" => show_ingress(&resource),
        "HorizontalPodAutoscaler" => show_horizontalpodautoscaler(&resource),
        "PersistentVolumeClaim" => show_persistentvolumeclaim(&resource),
        "Secret" => show_secret(&resource),
        "Service" => show_service(&resource),
        "ServiceAccount" => show_serviceaccount(&resource),
        _ => show_other(&resource),
    };
}

// Return svg icon of the resource
fn get_resource_icon(resource_short_name: &str) -> String {
    format!(
        "<img src='{}/{}.svg' {} />",
        ICON_BASE_URL, resource_short_name, ICON_CSS
    )
}

fn show_configmap(resource: &Resource) {
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}[(\"{} {}<br/><small>{}</small>\")]:::none",
            &resource.key,
            get_resource_icon("cm"),
            &resource.name,
            &resource.kind
        );
    } else {
        println!(
            "{}[(\"{}<br/><small>{}</small>\")]:::red",
            &resource.key, &resource.name, &resource.kind
        );
    }
}

fn show_deployment(resource: &Resource) {
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}(\"{} {}<br/><small>{}</small>\"):::none",
            &resource.key,
            get_resource_icon("deploy"),
            &resource.name,
            &resource.kind
        );
    } else {
        println!(
            "{}(\"{}<br/><small>{}</small>\"):::none",
            &resource.key, &resource.name, &resource.kind
        );
    }

    // Get "spec.replicas" property
    let value_spec_replicas = serde_yaml::to_string(&resource.yaml["spec"]["replicas"]);
    let spec_replicas = match value_spec_replicas {
        Ok(spec_replicas) => {
            if spec_replicas.trim() == "null" {
                String::from("0")
            } else {
                String::from(spec_replicas.trim())
            }
        }
        Err(e) => {
            eprintln!("Error: {}", e);
            std::process::exit(exitcode::DATAERR);
        }
    };

    // Create ReplicaSet and Pod
    let rs_suffix = get_random_suffix(8);
    if *RENDER_WITH_ICONS == true {
        println!(
            "{} --> {}_rs(\"{} {}-{}<br><small>ReplicaSet</small>\"):::none",
            &resource.key,
            &resource.key,
            get_resource_icon("rs"),
            &resource.name,
            rs_suffix
        );
    } else {
        println!(
            "{} --> {}_rs(\"{}-{}<br><small>ReplicaSet</small>\"):::blue",
            &resource.key, &resource.key, &resource.name, rs_suffix
        );
    }

    // Concatenate all pods in one
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}_pod(\"{} {}-{}-xxxxx<br><small><b>{}x</b> Pod</small>\"):::default",
            &resource.key,
            get_resource_icon("pod"),
            &resource.name,
            rs_suffix,
            spec_replicas
        );
    } else {
        println!(
            "{}_pod(\"{}-{}-xxxxx<br><small><b>{}x</b> Pod</small>\"):::blue",
            &resource.key, &resource.name, rs_suffix, spec_replicas
        );
    }
    println!("{}_rs -->|supervise| {}_pod", &resource.key, &resource.key);

    // Create link Pod ==> ServiceAccount
    let value_spec_serviceaccountname =
        serde_yaml::to_string(&resource.yaml["spec"]["template"]["spec"]["serviceAccountName"]);
    let spec_serviceaccountname = match value_spec_serviceaccountname {
        Ok(spec_serviceaccountname) => String::from(spec_serviceaccountname.trim()),
        Err(e) => {
            eprintln!("Error: {}", e);
            std::process::exit(exitcode::DATAERR);
        }
    };
    if !spec_serviceaccountname.is_empty() {
        println!(
            "{}_pod -.->|executed as| {}_ServiceAccount_{}",
            &resource.key, &resource.namespace, spec_serviceaccountname
        )
    }

    // Create link Deployment ==> EnvFrom
    let containers_sequence = &resource.yaml["spec"]["template"]["spec"]["containers"]
        .as_sequence()
        .unwrap();
    for (_i, container) in containers_sequence.iter().enumerate() {
        let envfroms_sequence = match container["envFrom"].as_sequence() {
            Some(envfroms_sequence) => envfroms_sequence,
            None => continue,
        };

        for (_i, envfrom) in envfroms_sequence.iter().enumerate() {
            // Create link Deployment ==> EnvFrom ConfigMap
            let yaml_as_str = serde_yaml::to_string(&envfrom["configMapRef"]["name"]).unwrap();
            if !(yaml_as_str.trim().is_empty() || yaml_as_str.trim() == "null") {
                println!(
                    "{}_pod -.-> |inject as<br/>environment variables| {}_ConfigMap_{}",
                    &resource.key,
                    &resource.namespace,
                    yaml_as_str.trim()
                );
            }

            // Create link Deployment ==> EnvFrom Secret
            let yaml_as_str = serde_yaml::to_string(&envfrom["secretRef"]["name"]).unwrap();
            if !(yaml_as_str.trim().is_empty() || yaml_as_str.trim() == "null") {
                println!(
                    "{}_pod -.-> |inject as<br/>environment variables| {}_Secret_{}",
                    &resource.key,
                    &resource.namespace,
                    yaml_as_str.trim()
                );
            }
        }
    }

    // Create link Deployment ==> Volumes
    match &resource.yaml["spec"]["template"]["spec"]["volumes"].as_sequence() {
        Some(volumes_sequence) => {
            for (_i, volume) in volumes_sequence.iter().enumerate() {
                // Create link Deployment ==> ConfigMap
                let yaml_as_str = serde_yaml::to_string(&volume["configMap"]["name"]).unwrap();
                if !(yaml_as_str.trim().is_empty() || yaml_as_str.trim() == "null") {
                    println!(
                        "{}_pod -.-> |mount as volume| {}_ConfigMap_{}",
                        &resource.key,
                        &resource.namespace,
                        yaml_as_str.trim()
                    );
                }

                // Create link Deployment ==> Secret
                let yaml_as_str = serde_yaml::to_string(&volume["secret"]["secretName"]).unwrap();
                if !(yaml_as_str.trim().is_empty() || yaml_as_str.trim() == "null") {
                    println!(
                        "{}_pod -.-> |mount as volume| {}_Secret_{}",
                        &resource.key,
                        &resource.namespace,
                        yaml_as_str.trim()
                    );
                }

                // Create link Deployment ==> PersistentVolumeClaim
                let yaml_as_str =
                    serde_yaml::to_string(&volume["persistentVolumeClaim"]["claimName"]).unwrap();
                if !(yaml_as_str.trim().is_empty() || yaml_as_str.trim() == "null") {
                    println!(
                        "{}_pod -.-> |mount as volume| {}_PersistentVolumeClaim_{}",
                        &resource.key,
                        &resource.namespace,
                        yaml_as_str.trim()
                    );
                }
            }
        }
        None => {}
    };
}

fn show_horizontalpodautoscaler(resource: &Resource) {
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}(\"{} {}<br/><small>{}</small>\"):::none",
            &resource.key,
            get_resource_icon("hpa"),
            &resource.name,
            &resource.kind
        );
    } else {
        println!(
            "{}(\"{}<br/><small>{}</small>\"):::red",
            &resource.key, &resource.name, &resource.kind
        );
    }
}

fn show_ingress(resource: &Resource) {
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}{{{{\"{} {}<br/><small>{}</small>\"}}}}:::none",
            &resource.key,
            get_resource_icon("ing"),
            &resource.name,
            &resource.kind
        );
    } else {
        println!(
            "{}{{{{{}<br/><small>{}</small>}}}}:::green",
            &resource.key, &resource.name, &resource.kind
        );
    }

    // Get "spec.rules" property
    let value_spec_rules = &resource.yaml["spec"]["rules"].as_sequence();
    let spec_rules = match value_spec_rules {
        Some(spec_rules) => spec_rules,
        None => {
            eprintln!("Error:");
            std::process::exit(exitcode::DATAERR);
        }
    };

    for (_i, rule) in spec_rules.iter().enumerate() {
        let ingress_host = serde_yaml::to_string(&rule["host"]).unwrap();

        // Get "http.paths" property
        let value_spec_paths = &rule["http"]["paths"].as_sequence();
        let spec_paths = match value_spec_paths {
            Some(spec_paths) => spec_paths,
            None => {
                eprintln!("Error:");
                std::process::exit(exitcode::DATAERR);
            }
        };

        for (_i, path) in spec_paths.iter().enumerate() {
            // Get "backend.service.name" property
            let service_name = serde_yaml::to_string(&path["backend"]["service"]["name"]).unwrap();
            if !(service_name.trim().is_empty() || service_name.trim() == "null") {
                // Get "backend.service.port.number" property
                let service_port =
                    serde_yaml::to_string(&path["backend"]["service"]["port"]["number"]).unwrap();
                if !(service_port.trim().is_empty() || service_port.trim() == "null") {
                    println!(
                        "{} -.->|{}| {}_Service_{}",
                        &resource.key,
                        ingress_host.trim(),
                        &resource.namespace,
                        service_name.trim()
                    );
                }
            }
        }
    }
}

fn show_persistentvolumeclaim(resource: &Resource) {
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}[(\"{} {}<br/><small>{}</small>\")]:::none",
            &resource.key,
            get_resource_icon("pvc"),
            &resource.name,
            &resource.kind
        );
    } else {
        println!(
            "{}[(\"{}<br/><small>{}</small>\")]:::red",
            &resource.key, &resource.name, &resource.kind
        );
    }
}

fn show_secret(resource: &Resource) {
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}[(\"{} {}<br/><small>{}</small>\")]:::none",
            &resource.key,
            get_resource_icon("secret"),
            &resource.name,
            &resource.kind
        );
    } else {
        println!(
            "{}[(\"{}<br/><small>{}</small>\")]:::red",
            &resource.key, &resource.name, &resource.kind
        );
    }
}

fn show_service(resource: &Resource) {
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}{{{{\"{} {}<br/><small>{} - port: XXXX</small>\"}}}}:::none",
            &resource.key,
            get_resource_icon("svc"),
            &resource.name,
            &resource.kind
        );
    } else {
        println!(
            "{}{{{{{}<br/><small>{} - port: XXXX</small>}}}}:::green",
            &resource.key, &resource.name, &resource.kind
        );
    }
}

fn show_serviceaccount(resource: &Resource) {
    if *RENDER_WITH_ICONS == true {
        println!(
            "{}[[\"{} {}<br/><small>{}</small>\"]]:::none",
            &resource.key,
            get_resource_icon("sa"),
            &resource.name,
            &resource.kind
        );
    } else {
        println!(
            "{}[[{}<br/><small>{}</small>]]",
            &resource.key, &resource.name, &resource.kind
        );
    }
}

fn show_other(resource: &Resource) {
    println!(
        "{}[[{}<br/><small>{}</small>]]:::grey",
        &resource.key, &resource.name, &resource.kind
    );
}

// Resolve links beetween Services and Pods from a Deployment
pub fn resolve_label_selectors(resource_vec: &Vec<Resource>) {
    let mut services_vec: Vec<&Resource> = Vec::new();
    let mut deployments_vec: Vec<&Resource> = Vec::new();

    for resource in resource_vec {
        if resource.namespace == *NAMESPACE {
            if resource.kind == "Service" {
                services_vec.push(resource);
            }
            if resource.kind == "Deployment" {
                deployments_vec.push(resource);
            }
        }
    }

    for service in services_vec {
        // Get "spec.selector" property
        let value_spec_selector = serde_yaml::to_value(&service.yaml["spec"]["selector"]);
        let spec_selector = match value_spec_selector {
            Ok(spec_selector) => spec_selector,
            Err(e) => {
                eprintln!("Error: {}", e);
                std::process::exit(exitcode::DATAERR);
            }
        };

        for deployment in &deployments_vec {
            // Get "spec.selector.matchLabels" property
            let value_d_spec_selector =
                serde_yaml::to_value(&deployment.yaml["spec"]["selector"]["matchLabels"]);
            let spec_d_selector = match value_d_spec_selector {
                Ok(spec_d_selector) => spec_d_selector,
                Err(e) => {
                    eprintln!("Error: {}", e);
                    std::process::exit(exitcode::DATAERR);
                }
            };

            // Create link Service ==> Pod
            if spec_selector == spec_d_selector {
                println!("{} -.->|target| {}_pod", service.key, deployment.key);
            }
        }
    }
}
