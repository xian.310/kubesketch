# Kubesketch

A tool to render your Kubernetes yaml manifests into [Mermaid](https://mermaid.js.org) or [D2](https://d2lang.com) diagrams.

Kubesketch can render your manifests like this:

![Example of Mermaid diagram generated with kubesketch](examples/example-no-icons.png)

Or using the official Kubernetes icons, like that:

![Another example of Mermaid diagram generated with kubesketch](examples/example-icons.png)

## Usage

```
$ ./target/release/kubesketch --help

Usage: kubesketch [OPTIONS] <PATH>

Arguments:
  <PATH>  The path to the file to read, use - to read from stdin (must not be a tty)

Options:
  -n, --namespace <NAMESPACE>  Kubernetes namespace [default: default]
  -f, --format <FORMAT>        Output format [default: mermaid]
  -i, --icons                  Use official kubernetes resources icons
  -q, --quiet                  Show diagram only
  -v, --verbose                Verbose mode
  -h, --help                   Print help
  -V, --version                Print version
```

## Examples

Kubesketch a manifest:

`$ kubesketch manifests/my-app.yaml`

Kubesketch a manifest and specify a namespace:

`$ kubesketch manifests/my-app.yaml --namespace hello`

Using stdin:

`$ cat manifests/my-app.yaml | kubesketch -`
